var socket = io.connect('http://192.168.0.10:6677', {'forceNew': true});

socket.on('messages', function(data) {
    console.log(data);
    render(data);
});

function render(data) {
    var d = new Date(),
        h = (d.getHours() < 10
            ? '0'
            : '') + d.getHours(),
        m = (d.getMinutes() < 10
            ? '0'
            : '') + d.getMinutes();
    var hour = h + ':' + m;
    var html = data.map(function(message, index) {
        return (`
            <li class="message list-group-item bg-faded">
                <strong>${message.nickname}: &nbsp;</strong>
                <span>${message.text}</span>
                <span class="hour text-muted">${hour}</span>
            </div>
        `);
    }).join(' ');
    var div_messages = document.getElementById('messages');
    div_messages.innerHTML = html;
    div_messages.scrollTop = div_messages.scrollHeight;
}

function addMessage(e) {

    var message = {
        nickname: document.getElementById('nickname').value,
        text: document.getElementById('message').value
    };

    document.getElementById('nickname').parentNode.style.display = 'none';
    document.getElementById('message').value = '';
    document.getElementById('message').focus();

    socket.emit('add-message', message);
    return false;
}
